---
layout: default
title: "Feature Gallery"
---

## Collect
<p>
<span class="image left"><img src="{{ site.base_url }}/images/collect.png"/></span>
Worried about forgetting something? Unexpected memory losses? Zanshin is here to help you.
You can collect your tasks and free your mind. You will soon stop hearing this
voice nagging you about what you probably forgot, or what you should do. Just fire up
Zanshin, once something is collected, you are sure to find it there.</p>
<br/>

## Organize
<p>
<span class="image right"><img src="{{ site.base_url }}/images/organize.png"/></span>
Zanshin makes it simple to put your daily life in order. Just create a few projects, a few
contexts and start filling them with tasks. You can simply do this using drag and drop.
Something has to be done before a given day? Just set a due date and it'll stand out from
your other items. You need to work on a project? Focus on it and you will see all the
related tasks. Feeling like dealing with your phone calls? Focus on your phone context to
find all the pending calls you have to give.</p>
<br/>

## Optimize
<p>
<span class="image left"><img src="{{ site.base_url }}/images/optimize.png"/></span>
We hackers love our keyboards, and you probably find yourself more efficient with your
hands on the keyboard as much as possible. That's why we crafted Zanshin carefully to
be completely drivable from the keyboard. We added shortcuts to add tasks, move them
to a project or a context, navigate, etc.</p>
<br/>


## Integrate
<p>
<span class="image right"><img src="{{ site.base_url }}/images/integrate.png"/></span>
Zanshin is an application of the KDE community, as such it unleashes even more power
when used with other KDE Applications or within KDE Plasma Desktop. Using KMail? You
can create a task right from an email you received and it'll appear in Zanshin's Inbox.
Using KOrganizer? All your overdue and coming task will appear in the calendar view.
Using Plasma Desktop? Then you can collect from anywhere using KRunner, just type in
"todo: Buy apples", and you will find that tasks in Zanshin's Inbox, even if it
wasn't running!</p>

