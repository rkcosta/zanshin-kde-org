---
layout: post
title: "Zanshin 0.2 RC1"
date: Mon Oct 10 22:35:23 +0200 2011
tags:
- Akonadi
- KDE
- Zanshin
---

A few weeks ago [we released Zanshin 0.2 beta2](/2011/08/31/zanshin-0.2-beta2 "Zanshin 0.2 beta2 release"),
and I'm glad to announce the immediate availability of Zanshin 0.2 RC1. Except if
any showstopper bug is reported, it will be the last stop before 0.2 final.

I'd also like to use the opportunity to report a few changes regarding
contributions and adoption. We've seen tremendous activity on the packaging
front since the previous release:

 - It is available for openSUSE and Gentoo as previously announced;
 - It is now available for Fedora thanks to Christoph Wickert of
   [Kolab Systems](http://www.kolabsys.com),
   you can grab it from
   [Christoph's repository](http://repos.fedorapeople.org/repos/cwickert/zanshin/)
   and it'll hopefully get into Fedora itself soon;
 - I got pointed out that it was already available in
   [Arch User Repository](http://aur.archlinux.org/packages.php?ID=52024);
 - Kartik Mistry volunteered to package it for Debian, so we'll have some good news there soon hopefully;
 - Patrick Spendrin confirmed to me that it got added to the KDE-Windows port, and so it was officially
   released with the KDE-Windows 4.7.0 release;
 - On Mac? I got users building it for themselves reporting it to work, but no
   official packaging yet.

I'm glad to see so many people stepping up like that, bringing some GTD goodness wrapped in
Free Software to more and more potential users.

And since some people pointed it out on my previous post, yes we need a website,
screenshots and so on. We've been aware of it for a while, but we've been too busy
working on the software itself. The feedback on Zanshin 0.2 beta2 didn't bring many
issues, so we used the extra time to work on a website. It's not ready for prime time
yet, but we hope to go live with 0.2 final.

If you want to get Zanshin from sources, the tarballs are available, at the same place than
usual on [files.kde.org](http://files.kde.org/zanshin "Zanshin tarballs").

And of course, you can still *git clone kde:zanshin* if you want the bleeding
edge or if you wish to contribute to the code.

Now we're waiting a bit for your feedback. We have exactly one minor bug left in
our list and the future website need some extra polish. Hopefully at this pace we
won't need a 0.2 RC2.

